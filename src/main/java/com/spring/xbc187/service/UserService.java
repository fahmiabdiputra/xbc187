package com.spring.xbc187.service;

import com.spring.xbc187.model.UserModel;

public interface UserService {

	public UserModel searchUsernamePassword(String username, String password);
}
