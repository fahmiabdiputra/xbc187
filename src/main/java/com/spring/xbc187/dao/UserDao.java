package com.spring.xbc187.dao;

import com.spring.xbc187.model.UserModel;

public interface UserDao {

	public UserModel searchUsernamePassword(String username, String password);
}
