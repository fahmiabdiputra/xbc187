
<nav class="navbar" style="height: 100%; position: fixed;"
	role="navigation">
	<aside class="main-sidebar">

		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header">Smart School</li>
					
				<li>
					<a href="#"><i class="fa fa-bars"></i><span>Master</span><span class="pull-right-container"> </span></a>
					<ul class="treeview-menu">
						<c:choose>
							<c:when test="${kodeRole=='ROLE_ADMIN'}">
								<li>
									<a href="${contextName}/Pelajaran.html" class="menu-item"><i class="fa fa-truck"></i>Pelajaran</a>
								</li>
							</c:when>
						</c:choose>
						<li>
							<a href="${contextName}/contoh.html" class="menu-item"><i class="fa fa-truck"></i>Contoh</a>
						</li>
						<li>
							<a href="${contextName}/barang.html" class="menu-item"><i class="fa fa-truck"></i>Barang</a>
						</li>	
								
					</ul>
				</li>	
				
				<li>
					<a href="#"><i class="fa fa-bars"></i> <span>Transaksi</span><span class="pull-right"></span></a>
					<ul class="treeview-menu">
						<li>
							<a href="#"><i class="fa fa-calculator"></i><span>Kasir</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/kasir_header.html" class="menu-item"><i class="fa fa-calculator"></i>Kasir</a>
								</li>
								<li>
									<a href="${contextName}/kasir_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
						
						<li>
							<a href="#"><i class="fa fa-shopping-cart"></i><span>Jasa</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/jasa_header.html" class="menu-item"><i class="fa fa-shopping-cart"></i>Jasa</a>
								</li>
								<li>
									<a href="${contextName}/jasa_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
						
						<li>
							<a href="#"><i class="fa fa-shopping-basket"></i><span>Gudang</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/gudang_header.html" class="menu-item"><i class="fa fa-shopping-basket"></i>Gudang</a>
								</li>
								<li>
									<a href="${contextName}/gudang_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
						
						<li>
							<a href="#"><i class="fa fa-mail-reply"></i><span>Retur</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/retur_header.html" class="menu-item"><i class="fa fa-mail-reply"></i>Retur</a>
								</li>
								<li>
									<a href="${contextName}/retur_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>	
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
</nav>